﻿using UnityEngine;
using System.Reflection;

public class LoseCollider : MonoBehaviour {
	[Tooltip("son lorsque le joueur perd la partie")]
	public AudioClip
		loseSound;
	LevelManager levelManager;
	
	void Start () {
		levelManager = GameObject.FindObjectOfType<LevelManager> ();
		
	}

	void OnTriggerEnter2D (Collider2D collider) {
		//Debug.Log ("Enter " + this.name + ":" + MethodBase.GetCurrentMethod ().Name);
		if (collider == null) {
			throw new System.ArgumentNullException ("collider");
		}
		Debug.Log ("collider " + collider.gameObject.name);
		if (loseSound) {
			AudioSource.PlayClipAtPoint (loseSound, transform.position);
			//TODO : le son est interrompu par le chargement du niveau suivant
		}
		levelManager.LoadLevel ("Lose Screen");
	}

	
}
